package com.training.fundtransfer.util;

public interface SuccessConstant {

	
	String ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE = "Successfully Executed";
	int ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE = 2000;
	
	String AMOUNT_TRANSFERED_SUCCESSFULL_MESSAGE = "Amount Transefered successfully";
	int AMOUNT_TRANSFERED_SUCCESSFULL_CODE = 2001;
}
