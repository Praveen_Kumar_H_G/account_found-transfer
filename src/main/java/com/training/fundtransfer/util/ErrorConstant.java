package com.training.fundtransfer.util;

public interface ErrorConstant {
	
	String NO_RECORD_FOUND_MESSAGE = "No Record Found";
	int CODE_NO_RECORD_FOUND = 1001;
	
	String FROM_ACCOUNT_NOT_FOUND_MESSAGE = "From Account Number Not Found";
	int FROM_ACCOUNT_NOT_FOUND_CODE = 1002;
	
	String TO_ACCOUNT_NOT_FOUND_MESSAGE = "Transfer Account Number Not Found";
	int TO_ACCOUNT_NOT_FOUND_CODE = 1003;
	
	String INSUFFICIENT_AMOUNT_MESSAGE = "Insufficient Amount";
	int INSUFFICIENT_AMOUNT_CODE = 1004;

}
