package com.training.fundtransfer.entity;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "accounts")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id")
	private long accountId;
	@Column(name = "account_number")
	private long accountNumber;
	@Column(name = "account_name")
	private String accountHolderName;
	@Column(name = "balance")
	private BigDecimal balance;
	@Column(name = "ifc_code")
	private String ifcCode;
	@Column(name = "bank_name")
	private String bankName;

}
