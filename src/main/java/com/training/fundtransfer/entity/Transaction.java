package com.training.fundtransfer.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transactions")
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	private long transactionId;
	@Column(name = "from_account_number")
	private long fromAccountNumber;
	@Column(name = "to_account_number")
	private long toAccountNumber;
	@Column(name = "transaction_date")
	@CreationTimestamp
	private LocalDateTime transactionDate;
	@Column(name = "descrption")
	private String descrption;
	@Column(name = "amount")
	private BigDecimal amount;

}
