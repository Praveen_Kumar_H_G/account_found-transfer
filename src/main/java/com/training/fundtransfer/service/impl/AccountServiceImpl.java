package com.training.fundtransfer.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.training.fundtransfer.dto.AccountDetailsResponseDto;
import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.repository.AccountRepository;
import com.training.fundtransfer.service.AccountService;
import com.training.fundtransfer.util.SuccessConstant;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author praveenakumara.hitne
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
	
	private final AccountRepository accountRepository;
	/**
	 * fetch the account details in descending order
	 * @return response as successCode and successMessage
	 */
	@Override
	public AccountDetailsResponseDto getAllAccountNumberDescending() {
		List<Account> accountNumberDesc = accountRepository.findAllByOrderByAccountNumberDesc();
		AccountDetailsResponseDto accountDetailsResponseDto = AccountDetailsResponseDto.builder()
				.accountList(accountNumberDesc)
				.statusCode(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE)
				.statusMessage(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE)
				.build();
		log.info("Account details fetched successfully");
		return accountDetailsResponseDto;
	}

}
