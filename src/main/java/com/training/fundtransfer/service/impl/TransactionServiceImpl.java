package com.training.fundtransfer.service.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.training.fundtransfer.dto.ApiResponse;
import com.training.fundtransfer.dto.TransactionDto;
import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.entity.Transaction;
import com.training.fundtransfer.exception.AccountNotFound;
import com.training.fundtransfer.exception.InsufficientAmount;
import com.training.fundtransfer.repository.AccountRepository;
import com.training.fundtransfer.repository.TransactionRepository;
import com.training.fundtransfer.service.TransactionService;
import com.training.fundtransfer.util.ErrorConstant;
import com.training.fundtransfer.util.SuccessConstant;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

	private final AccountRepository accountRepository;
	private final TransactionRepository transactionRepository;
	
	/**
	 * Transfer Amount to another account
	 * @param fromAccountNumber get the payee's account number
	 * @param transactionDto as contains transfer account number and amount
	 * @return Response as contains Success code and message
	 */
	
	
	@Override
	public ApiResponse transferAmountToAccount(long fromAccountNumber, TransactionDto transactionDto) {
		Account fromAccount = accountRepository.findByAccountNumber(fromAccountNumber).orElseThrow(() -> {
			log.error("From Account not found");
			throw new AccountNotFound(ErrorConstant.FROM_ACCOUNT_NOT_FOUND_MESSAGE,ErrorConstant.FROM_ACCOUNT_NOT_FOUND_CODE);
		});
		Account toAccount = accountRepository.findByAccountNumber(transactionDto.toAccountNumber()).orElseThrow(() -> {
			log.error("Transfer account number not found");
			throw new AccountNotFound(ErrorConstant.TO_ACCOUNT_NOT_FOUND_MESSAGE,ErrorConstant.TO_ACCOUNT_NOT_FOUND_CODE);
		});
		int comparison = fromAccount.getBalance().compareTo(transactionDto.amount());
		if (comparison < 0) {
			log.error("Insufficient Amount");
			throw new InsufficientAmount();
		}else {
			Transaction transaction = Transaction.builder()
					.amount(transactionDto.amount())
					.descrption(transactionDto.descrption())
					.fromAccountNumber(fromAccountNumber)
					.toAccountNumber(transactionDto.toAccountNumber())
					.build();
			transactionRepository.save(transaction);
			BigDecimal result = fromAccount.getBalance().subtract(transactionDto.amount());
			fromAccount.setBalance(result);
			accountRepository.save(fromAccount);
			toAccount.setBalance(toAccount.getBalance().add(transactionDto.amount()));
			accountRepository.save(toAccount);
		}
		return ApiResponse.builder()
				.statusCode(SuccessConstant.AMOUNT_TRANSFERED_SUCCESSFULL_CODE)
				.statusMessage(SuccessConstant.AMOUNT_TRANSFERED_SUCCESSFULL_MESSAGE)
				.build();
	}

}
