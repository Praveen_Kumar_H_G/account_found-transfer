package com.training.fundtransfer.service;

import com.training.fundtransfer.dto.AccountDetailsResponseDto;

public interface AccountService {

	AccountDetailsResponseDto getAllAccountNumberDescending();

}
