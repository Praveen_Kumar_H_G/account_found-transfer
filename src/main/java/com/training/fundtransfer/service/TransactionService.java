package com.training.fundtransfer.service;

import com.training.fundtransfer.dto.ApiResponse;
import com.training.fundtransfer.dto.TransactionDto;

public interface TransactionService {

	ApiResponse transferAmountToAccount(long fromAccountNumber, TransactionDto transactionDto);

}
