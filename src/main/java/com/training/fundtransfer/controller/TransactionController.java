package com.training.fundtransfer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.fundtransfer.dto.ApiResponse;
import com.training.fundtransfer.dto.TransactionDto;
import com.training.fundtransfer.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author praveenakumara.hitne
 */

@Slf4j
@RequestMapping("/api/v1")
@RestController
@RequiredArgsConstructor
public class TransactionController {
	
	private final TransactionService transactionService;
	/**
	 * Transfer amount one account to another account.
	 * @param fromAccountNumber get the payee's account number.
	 * @param transactionDto as contain receiver account number and amount.
	 * @return response as successCode and successMessage.

	 */
	@PostMapping("/transactions")
	public ResponseEntity<ApiResponse> transferAmount(@RequestParam ("fromAccountNumber") long fromAccountNumber,
		@Valid	@RequestBody TransactionDto transactionDto){
		log.info("Amount Transfer to ToAccount");
		return new ResponseEntity<>(transactionService.transferAmountToAccount(fromAccountNumber,transactionDto),HttpStatus.OK);
	}

}
