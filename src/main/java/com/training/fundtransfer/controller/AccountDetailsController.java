package com.training.fundtransfer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.fundtransfer.dto.AccountDetailsResponseDto;
import com.training.fundtransfer.service.AccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


/**
 * @author praveenakumara.hitne
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Slf4j
public class AccountDetailsController {
	
	private final AccountService accountService;
	
	/**
	 * get all account number descending order
	 * @return Response success code and message
	 */
	
	@GetMapping("/accounts")
	public ResponseEntity<AccountDetailsResponseDto> getAllAccountNumberDescending(){
		log.info("Account details controller statred");
		return new ResponseEntity<>(accountService.getAllAccountNumberDescending(),HttpStatus.OK);
	}
}
