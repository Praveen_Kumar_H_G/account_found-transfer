package com.training.fundtransfer.exception;

import com.training.fundtransfer.util.ErrorConstant;

public class InsufficientAmount extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientAmount() {
		super(ErrorConstant.INSUFFICIENT_AMOUNT_MESSAGE,ErrorConstant.INSUFFICIENT_AMOUNT_CODE);
	}

}
