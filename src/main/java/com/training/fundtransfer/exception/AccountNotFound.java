package com.training.fundtransfer.exception;

public class AccountNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFound(String error,int code) {
		super(error, code);
	}

}
