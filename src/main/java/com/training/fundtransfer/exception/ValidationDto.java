package com.training.fundtransfer.exception;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationDto {
	private Map<String, String> validationErrors;
	private String errorCode;
	private String errorMessage;

}
