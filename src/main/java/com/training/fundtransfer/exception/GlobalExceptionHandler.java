package com.training.fundtransfer.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		ValidationDto validationDto = new ValidationDto();
		validationDto.setValidationErrors(errors);
		validationDto.setErrorCode("FSA0003");
		validationDto.setErrorMessage("Please add valid input data");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationDto);
	}

	@ExceptionHandler(AccountNotFound.class)
	protected ResponseEntity<ErrorResponse> handleGlobalException(AccountNotFound accountNotFound) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
				ErrorResponse.builder().code(accountNotFound.getCode()).message(accountNotFound.getMessage()).build());
	}

	@ExceptionHandler(InsufficientAmount.class)
	protected ResponseEntity<ErrorResponse> handleGlobalException(InsufficientAmount insufficientAmount) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(insufficientAmount.getCode()).message(insufficientAmount.getMessage()).build());
	}

}
