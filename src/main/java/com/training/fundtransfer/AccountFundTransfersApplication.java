package com.training.fundtransfer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.repository.AccountRepository;

import lombok.RequiredArgsConstructor;

@SpringBootApplication
@RequiredArgsConstructor
public class AccountFundTransfersApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AccountFundTransfersApplication.class, args);
	}

	private final AccountRepository accountRepository;

	@Override
	public void run(String... args) {
		saveAccountDetails();

	}
	private String ifcCode  = "SBI00007N";
	private String bank= "State Bank Of India";

	public void saveAccountDetails() {
		
		
		List<Account> listAccounts = new ArrayList<>();

		Account account = Account.builder().accountId(1l).accountNumber(12345678908l).accountHolderName("Ankitha")
				.balance(BigDecimal.valueOf(2000.89)).ifcCode(ifcCode).bankName(bank).build();

		Account account1 = Account.builder().accountId(2l).accountNumber(12345678909l).accountHolderName("Sireesha")
				.balance(BigDecimal.valueOf(1000.89)).ifcCode(ifcCode).bankName(bank).build();
		Account account2 = Account.builder().accountId(3l).accountNumber(12345678911l).accountHolderName("Niharika")
				.balance(BigDecimal.valueOf(3000.89)).ifcCode(ifcCode).bankName(bank).build();
		Account account4 = Account.builder().accountId(4l).accountNumber(12345678912l).accountHolderName("Sooraj")
				.balance(BigDecimal.valueOf(2000.89)).ifcCode(ifcCode).bankName(bank).build();
		listAccounts.add(account);
		listAccounts.add(account1);
		listAccounts.add(account2);
		listAccounts.add(account4);
		accountRepository.saveAll(listAccounts);

	}

}
