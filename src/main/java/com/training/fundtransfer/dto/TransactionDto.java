package com.training.fundtransfer.dto;

import java.math.BigDecimal;

import lombok.Builder;
@Builder
public record TransactionDto(long toAccountNumber,
		BigDecimal amount,
		String descrption) {

}
