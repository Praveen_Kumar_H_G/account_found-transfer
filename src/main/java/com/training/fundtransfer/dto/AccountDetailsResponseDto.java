package com.training.fundtransfer.dto;

import java.util.List;

import com.training.fundtransfer.entity.Account;

import lombok.Builder;
@Builder
public record AccountDetailsResponseDto(int statusCode, 
		String statusMessage, 
		List<Account> accountList) {

}
