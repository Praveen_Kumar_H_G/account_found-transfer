package com.training.fundtransfer.dto;

import lombok.Builder;

@Builder
public record AccountResponseDto(String accountHolderName,
		long accountNumber) {

}
