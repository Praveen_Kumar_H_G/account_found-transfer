package com.training.fundtransfer.dto;

import lombok.Builder;

@Builder
public record ApiResponse(int statusCode,
		String statusMessage) {

}
