package com.training.fundtransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.fundtransfer.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
