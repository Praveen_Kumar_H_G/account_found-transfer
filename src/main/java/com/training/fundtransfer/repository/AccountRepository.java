package com.training.fundtransfer.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.fundtransfer.entity.Account;


public interface AccountRepository extends JpaRepository<Account, Long> {
	
	Optional<Account>  findByAccountNumber(long accountNumber);
	
	List<Account> findAllByOrderByAccountNumberDesc();
}
