package com.training.fundtransfer.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.fundtransfer.dto.AccountDetailsResponseDto;
import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.service.AccountService;
import com.training.fundtransfer.util.SuccessConstant;

@ExtendWith(SpringExtension.class)
class AccountDetailsControllerTest {
	@InjectMocks
	private AccountDetailsController controller;
	
	@Mock
	private AccountService service;
	
	@Test
	void getAllAccountDetailsSuccess() {
		Account account = Account.builder()
				.accountId(1l)
				.accountNumber(123456789l)
				.accountHolderName("Praveen")
				.build();
		List<Account> listAccount = new ArrayList<>();
		listAccount.add(account);
		AccountDetailsResponseDto responseDto = AccountDetailsResponseDto.builder().statusCode(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE)
				.statusMessage(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE)
				.accountList(listAccount).build();
		Mockito.when(service.getAllAccountNumberDescending()).thenReturn(responseDto);
		ResponseEntity<AccountDetailsResponseDto> allAccountNumberDescending = controller.getAllAccountNumberDescending();
		assertEquals(HttpStatus.OK, allAccountNumberDescending.getStatusCode());
	}

}
