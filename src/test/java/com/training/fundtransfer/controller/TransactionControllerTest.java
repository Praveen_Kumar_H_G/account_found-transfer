package com.training.fundtransfer.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.fundtransfer.dto.ApiResponse;
import com.training.fundtransfer.dto.TransactionDto;
import com.training.fundtransfer.service.TransactionService;
import com.training.fundtransfer.util.SuccessConstant;

@ExtendWith(SpringExtension.class)
class TransactionControllerTest {
	@InjectMocks
	private TransactionController controller;
	
	@Mock
	private TransactionService service;
	
	@Test
	void trasferAmountSuccess() {
		long fromAccountNum= 123456789l;
		TransactionDto transactionDto = TransactionDto.builder()
				.amount(BigDecimal.valueOf(2989.89))
				.toAccountNumber(1234567891l)
				.descrption("NA")
				.build();
		ApiResponse api = ApiResponse.builder()
				.statusCode(SuccessConstant.AMOUNT_TRANSFERED_SUCCESSFULL_CODE)
				.statusMessage(SuccessConstant.AMOUNT_TRANSFERED_SUCCESSFULL_MESSAGE)
				.build();
		Mockito.when(service.transferAmountToAccount(fromAccountNum, transactionDto)).thenReturn(api);
		ResponseEntity<ApiResponse> transferAmount = controller.transferAmount(fromAccountNum, transactionDto);
		assertEquals(HttpStatus.OK, transferAmount.getStatusCode());
		
	}

}
