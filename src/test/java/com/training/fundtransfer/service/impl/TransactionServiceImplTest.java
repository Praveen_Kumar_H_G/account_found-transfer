package com.training.fundtransfer.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.fundtransfer.dto.ApiResponse;
import com.training.fundtransfer.dto.TransactionDto;
import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.exception.AccountNotFound;
import com.training.fundtransfer.exception.InsufficientAmount;
import com.training.fundtransfer.repository.AccountRepository;
import com.training.fundtransfer.repository.TransactionRepository;
import com.training.fundtransfer.util.SuccessConstant;

@ExtendWith(SpringExtension.class)
class TransactionServiceImplTest {
	@InjectMocks
	private TransactionServiceImpl serviceImpl;
	
	@Mock
	private TransactionRepository transactionRepository;
	
	@Mock
	private AccountRepository accountRepository;
	
	long fromAccount= 12345678;
	long toAccount= 123456789;
	
	Account account = Account.builder()
			.accountHolderName("Praveen")
			.accountId(1l)
			.accountNumber(fromAccount)
			.balance(BigDecimal.valueOf(1000.99))
			.ifcCode("SBI00007N")
			.bankName("SBI")
			.build();
	Account account2 = Account.builder()
			.accountHolderName("Praveen")
			.accountId(1l)
			.accountNumber(fromAccount)
			.balance(BigDecimal.valueOf(100000.99))
			.ifcCode("SBI00007N")
			.bankName("SBI")
			.build();
	Account account1 = Account.builder()
			.accountHolderName("Praveen1")
			.accountId(2l)
			.accountNumber(toAccount)
			.balance(BigDecimal.valueOf(10000.99))
			.ifcCode("SBI00007N")
			.bankName("SBI")
			.build();
	
	TransactionDto dto = TransactionDto.builder()
			.toAccountNumber(toAccount)
			.amount(BigDecimal.valueOf(2993.33))
			.descrption("NA")
			.build();
	
	@Test
	void fromAccountNotFound() {
		Mockito.when(accountRepository.findByAccountNumber(fromAccount)).thenReturn(Optional.empty());
		assertThrows(AccountNotFound.class,()-> serviceImpl.transferAmountToAccount(fromAccount, dto));
	}
	@Test
	void trasferAccountNotFound() {
		Mockito.when(accountRepository.findByAccountNumber(fromAccount)).thenReturn(Optional.of(account));
		Mockito.when(accountRepository.findByAccountNumber(dto.toAccountNumber())).thenReturn(Optional.empty());
		assertThrows(AccountNotFound.class, ()-> serviceImpl.transferAmountToAccount(fromAccount, dto));
	}
	
	@Test
	void amountInsufficient() {
		Mockito.when(accountRepository.findByAccountNumber(fromAccount)).thenReturn(Optional.of(account));
		Mockito.when(accountRepository.findByAccountNumber(dto.toAccountNumber())).thenReturn(Optional.of(account1));
		assertThrows(InsufficientAmount.class, ()-> serviceImpl.transferAmountToAccount(fromAccount, dto));
	}
	
	@Test
	void amountTrasferSuccessfully() {
		Mockito.when(accountRepository.findByAccountNumber(fromAccount)).thenReturn(Optional.of(account2));
		Mockito.when(accountRepository.findByAccountNumber(dto.toAccountNumber())).thenReturn(Optional.of(account1));
		ApiResponse transferAmountToAccount = serviceImpl.transferAmountToAccount(fromAccount, dto);
		assertEquals(SuccessConstant.AMOUNT_TRANSFERED_SUCCESSFULL_MESSAGE, transferAmountToAccount.statusMessage());
	}

}
