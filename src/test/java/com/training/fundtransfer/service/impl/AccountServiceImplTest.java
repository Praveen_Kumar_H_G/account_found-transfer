package com.training.fundtransfer.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.fundtransfer.dto.AccountDetailsResponseDto;
import com.training.fundtransfer.entity.Account;
import com.training.fundtransfer.repository.AccountRepository;
import com.training.fundtransfer.util.SuccessConstant;

@ExtendWith(SpringExtension.class)
class AccountServiceImplTest {
	@InjectMocks
	private AccountServiceImpl impl;
	
	@Mock
	private AccountRepository repository;
	
	@Test
	void getAllAccountDetailsSuccessfully() {
		Account account = Account.builder()
				.accountId(1l)
				.accountNumber(123456789l)
				.accountHolderName("Praveen")
				.build();
		List<Account> listAccount = new ArrayList<>();
		listAccount.add(account);
		Mockito.when(repository.findAllByOrderByAccountNumberDesc()).thenReturn(listAccount);
		AccountDetailsResponseDto dto = impl.getAllAccountNumberDescending();
		assertEquals(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE, dto.statusMessage());
	}

}
